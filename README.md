# Interface extension

A FreshRSS extension which improve and test new elements in the interface.

To use it, upload the `xExtension-Interface/` directory in your `./extensions`
directory and enable it on the extension panel in FreshRSS.

**Important:** I don't work anymore on this extension which is finished. I
don't plan to fix potential bugs either (highly expected since FreshRSS has
changed a lot since the last time I worked on this extension). Feel free to
fork the project and adapt to your needs.

## License

[freshrss-interface](https://framagit.org/marienfressinaud/freshrss-interface)
is licensed under [AGPL 3](LICENSE).
